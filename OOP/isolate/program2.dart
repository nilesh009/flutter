Future<String?> getOrder() {
  return Future.delayed(Duration(seconds:5),()=>"chicken Burger");
}

Future<String?> getOrderMessage() async {

  var order=await getOrder();

  return "Your Order is $order";
}

void main() async{
  print("start");

  print(await getOrderMessage());

  print("End");
}