import 'dart:io';

Future<String?> getOrder() {
  stdout.write("Enter the Order :");
  String? ord=stdin.readLineSync();
  print("Processing...");
  return Future.delayed(Duration(seconds:5),()=>ord);
}

Future <String?> getOrderMessage() async {
  
  var order=await getOrder();

  return "Your $order is Ready";
}
void main() async{
  
  print("Welcome");

  print(await getOrderMessage());

  print("Visit Again");
}