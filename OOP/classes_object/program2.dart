class Demo {
  int x=10;
  static int y=20;

  void function() {
    print(x);
    print(y);
  }
}
void main() {
  Demo obj1=new Demo();
  Demo obj2=new Demo();

  obj1.function();
  obj2.function();

  obj1.x=60;
  
  obj1.function();
  obj2.function();

}