class Demo {
  int? x;
  String? str;

  Demo(int x,String str) {      //internal call-->Demo(this) {}
    this.x=x;
    this.str=str;
    print(this.hashCode);
  }
  void printData() {
    print(x);
    print(str);
  }
}

void main() {
  Demo obj=Demo(10,'Nilesh');    // internal call-->Demo obj=new Demo(obj);
  print(obj.hashCode);
  obj.printData();
}