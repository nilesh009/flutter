class parent {
    int x=10;
    parent() {
      print("In Parent constructor");
    }
    void printdata() {
      print(x);
    } 
}
class child extends parent{
  int x=30;
  child() {
    print("In child constructor");
  }
  void dispData() {
    print(x);
  }
}

void main() {
  child obj=new child();
  obj.dispData();
  obj.printdata();
}
