class parent {
    int x=10;
    parent() {
        super();
      System.out.println("In Parent constructor");
    }
    void printData() {
      System.out.println(x);
    } 
}

class child extends parent{
  int x=20;
  child() {
    System.out.println("In child constructor");
  }
  void dispData() {
    System.out.println(x);
  }
}

class Demo {
  public static void main(String []args) {
  child obj=new child();
  obj.dispData();
  obj.printData();
  }
}
