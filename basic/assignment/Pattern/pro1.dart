/* 10  20  30  40  50
   60  70  80  90  100 
   101 102 103 104 105
   106 107 108 109 110 
   120 130 140 150 160 */

import 'dart:io';

void main() {
  print('Enter the number of rows:');
  int rowCount = 5;

  printPattern(rowCount);
}

void printPattern(int rowCount) {
  int currentValue = 10;

  for (int i = 0; i < rowCount; i++) {
    for (int j = 0; j < 5; j++) {
      if (currentValue % 110 == 0) {
        currentValue += 10;
      }
      stdout.write('${currentValue + j}\t');
    }
    currentValue += 10;
    print('');
  }
}
