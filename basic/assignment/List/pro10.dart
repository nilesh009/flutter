// take 2 list from user and take list3 as output of list1 + list2

import 'dart:io';

void main() {
  stdout.write("Enter length of lists :");
  int lengthofList=int.parse(stdin.readLineSync()!);

  List<int> list1=[];  
  List<int?> list2=List<int?>.filled(lengthofList,null);

  print("Enter element of list:");

  for(int i=0;i<lengthofList;i++) {
    list1.add(int.parse(stdin.readLineSync()!));
    
    list2[(lengthofList-1) - i]=list1[i];
  }

  print("Original List= $list1");
  print("Reversed= $list2");

}
