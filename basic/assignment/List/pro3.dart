//cheak the list and print num which are divisible by 3 & 5

import 'dart:io';

void main() {
  List<int> list1=[];
  int count=0;

  print("Enter the element:");
  for(int i=0;i<6;i++) {
    list1.add(int.parse(stdin.readLineSync()!));  
  }

  for(int i=0;i<6;i++) {
    if(list1[i]%3==0 && list1[i]%5==0)
        count++;
  }

  if(count==6){
    print("True");
  }else{
    print("False");
  }
}