//find maximum in list
import 'dart:io';

void main() {

  List<int> list1=[];

  print("Enter the elements:");

  for(int i=0;i<6;i++) {
      list1.add(int.parse(stdin.readLineSync()!));
  }

  int max=list1[0];
  for(int i=1;i<6;i++) {
    if(list1[i]>max)
          max=list1[i];
  }

  print("maximum element : $max");
}