// search element
import 'dart:io';

void main () {
  stdout.write("Enter the length of list :");
  int length=int.parse(stdin.readLineSync()!);

  List<int> list1=[];
  print("enter the elements:");
  for(int i=0;i<length;i++) {
    list1.add(int.parse(stdin.readLineSync()!));
  }

  int count=0;
  stdout.write("Enter num to search:");
  int search=int.parse(stdin.readLineSync()!);
  for(int i=0;i<length;i++) {
      if(search==list1[i]) {
            count++;
            print("$search is found at index $i");
            break;
      }
  }
  if(count ==0)
    print("$search is not present in list");
}