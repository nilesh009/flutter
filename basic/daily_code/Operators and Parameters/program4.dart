void main() {
  int x=43;
  int y=72;

  print(x & y); //bitwise AND operator

  print(x | y); //bitwise OR operator

  print(x ^ y); //bitwise XOR operator

  print(x << 3); //left shift

  print(y >> 4); //Right shift
}