void main() {
  dynamic x=100;
  dynamic y=200;

  print(x.runtimeType);
  print(y.runtimeType);

  x=100.50;
  y=200.50;
  
  print(x.runtimeType);
  print(y.runtimeType);
}